import axios from 'axios';
import { store } from '../store/store'

const axiosInstance = axios.create({
  baseURL: 'http://192.168.1.242:8000/',
});
console.log(store.state.token);


axiosInstance.interceptors.request.use(
  (config) => {
    let token = store.state.token;

    if (token) {
      config.headers['Authorization'] = `Token ${token}`
    }

    return config
  },

  (error) => {
    return Promise.reject(error)
  }
);

export default axiosInstance;
