import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import Cookies from 'js-cookie'

Vue.use(Vuex);
axios.defaults.baseURL = 'http://192.168.1.242:8000/api/';

export const store = new Vuex.Store({
  state: {
    token: Cookies.get('token') || null
  },
  mutations: {
    logIn(state, token){
      state.token = token;
    }
  }
});
