import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import UserLoginForm from '@/components/UserLoginForm'
import TransactionList from '@/components/TransactionList'
import TransactionDetail from '@/components/TransactionDetail'
import TransactionForm from '@/components/TransactionForm'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'UserLoginForm',
      component: UserLoginForm,
      props: true
    },
    {
      path: '/transactions',
      name: 'TransactionList',
      component: TransactionList
    },
    {
      path: '/transactions/:transactionID',
      name: 'TransactionDetail',
      component: TransactionDetail,
      props: true
    },
    {
      path: '/create',
      name: 'TransactionForm',
      component: TransactionForm
    },
  ],
  mode: 'history'
})
