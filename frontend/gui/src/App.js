import React, {Component} from 'react';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux'
import BaseRoute from './routes';
import NawMenu from './components/NavMenu'
import * as actions from './store/actions/auth'

class App extends Component {
    render() {
        return (
            <div className="App">
                <NawMenu {...this.props} />
                <BrowserRouter>
                    <div>
                        <BaseRoute/>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        isAuthenticated: state.token !== null
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onTryAutoSignup: () => dispatch(actions.authCheckState())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
