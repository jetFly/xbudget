import React, {Component} from 'react';
import axios from 'axios'
import {Col, Row, Button, Form, FormGroup, Label, Input, Container} from 'reactstrap';
import TransactionCategories from './TransactionCategories'

class TransactionForm extends Component {

    state = {
        transactions: []
    };

    handleFormSubmit = (event, requestType, transactionID) => {
        event.preventDefault();
        const amount = event.target.elements.amount.value;
        const comment = event.target.elements.comment.value;
        const category = event.target.elements.category.value;

        switch (requestType) {
            case 'add':
                console.log('adding new...');
                axios.post('http://192.168.1.242:8000/api/transactions/', {
                    amount: amount,
                    //category: category,
                    comment: comment
                }).catch(error => console.error(error));
                break;
            case 'update':
                console.log('updating ...');
                axios.put(`http://192.168.1.242:8000/api/transactions/${transactionID}/`, {
                    amount: amount,
                    category: category,
                    comment: comment
                }).catch(error => console.error(error));
                break;
            default:
                console.log('form is wrong');
                break;
        }
    };

    render() {
        return (
            <Container>
                <Row>
                    <Form onSubmit={(event) => this.handleFormSubmit(
                        event,
                        this.props.requestType,
                        this.props.transactionID
                    )}>
                        <Row form>
                            <Col md={3}>
                                <FormGroup>
                                    <Label for="amount">Сумма</Label>
                                    <Input type="text" name="amount" id="amount"/>
                                </FormGroup>
                            </Col>
                            <Col md={5}>
                                <TransactionCategories/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label for="comment">Описание</Label>
                                    <Input type="textarea" name="comment" id="comment"/>
                                </FormGroup>
                            </Col>
                        </Row>

                        <Button type="submit">Готово</Button>
                    </Form>
                </Row>
            </Container>
        )
    }
}

export default TransactionForm;