import React, {Component} from 'react';
import axios from 'axios'
import {Container, Row, Col, ListGroup} from 'reactstrap';

import Transaction from './Transaction'

class TransactionList extends Component {

    state = {
        transactions: []
    }

    componentWillMount() {
        axios.get('http://192.168.1.242:8000/api/transactions/')
            .then(res => {
                this.setState({
                    transactions: res.data
                });
            })
    }

    render() {
        const transactionElements = this.state.transactions.map(transaction =>
            <Transaction key={transaction.id} transaction={transaction}/>
        );
        return (
            <Container>
                <Row>
                    <Col xs="2"/>
                    <Col xs="8">
                        <ListGroup>{transactionElements}</ListGroup>
                    </Col>
                    <Col xs="2"/>
                </Row>
            </Container>
        )
    }
}

export default TransactionList;