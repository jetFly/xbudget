import React, {Component} from 'react';
import axios from 'axios'
import {Container, Row, Col, Jumbotron, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge} from 'reactstrap';

class TransactionDetail extends Component {

    state = {
        transaction: {},
        dropdownOpen: false
    }

    toggle = this.toggle.bind(this);

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    componentWillMount() {
        const transactionID = this.props.match.params.transactionID;
        axios.get(`http://192.168.1.242:8000/api/transactions/${transactionID}/`)
            .then(res => {
                this.setState({
                    transaction: res.data
                });
                console.log(res.data)
            })
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <Jumbotron>
                            <h1 className="display-3">{this.state.transaction.amount}</h1>
                            <p className="lead"><Badge color="secondary">{this.state.transaction.category_name}</Badge> {this.state.transaction.comment}</p>
                            <hr className="my-2"/>
                            <p className="lead">
                                <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} className="float-right">
                                    <DropdownToggle caret>
                                        Действия
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem>Редактировать</DropdownItem>
                                        <DropdownItem divider/>
                                        <DropdownItem>Удалить</DropdownItem>
                                    </DropdownMenu>
                                </ButtonDropdown>
                            </p>
                        </Jumbotron>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default TransactionDetail;