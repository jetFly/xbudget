import React, {Component} from 'react';
import axios from 'axios'
import {Input, FormGroup, Label} from 'reactstrap';


class TransactionCategories extends Component {

    state = {
        categories: []
    }

    componentWillMount() {
        axios.get('http://192.168.1.242:8000/api/categories/')
            .then(res => {
                this.setState({
                    categories: res.data
                });
            })
    }

    render() {
        const transactionCategories = this.state.categories.map(categories =>
            <option key={categories.id} value={categories.id}>{categories.name}</option>
        );
        return (
            <FormGroup>
                <Label for="category">Категория</Label>
                <Input type="select" name="category" id="category">
                    {transactionCategories}
                </Input>
            </FormGroup>
        )
    }
}

export default TransactionCategories;