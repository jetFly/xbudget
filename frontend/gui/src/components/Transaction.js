import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {ListGroupItem, ListGroupItemHeading, ListGroupItemText} from 'reactstrap';


const Transaction = (props) => {
    let amountText = props.transaction.amount;
    let itemColor = '';
    if (props.transaction.transaction_type_code === 'I') {
        amountText = "+" + amountText;
        itemColor = 'success';
    } else {
        amountText = "-" + amountText;
    }
    return (
        <ListGroupItem color={itemColor} tag="a" href={'tran/' + props.transaction.id} action>
            <ListGroupItemHeading>{amountText}</ListGroupItemHeading>
            <ListGroupItemText>{(new Date(props.transaction.created)).toDateString()}</ListGroupItemText>
        </ListGroupItem>
    )
}

export default Transaction;
