import * as actioonTypes from './actionTypes';
import axios from 'axios';

export const authStart = () => {
    return {
        type: actioonTypes.AUTH_START
    }
};

export const authSuccess = (token) => {
    return {
        type: actioonTypes.AUTH_SUCCESS
    }
};

export const authFail = (error) => {
    return {
        type: actioonTypes.AUTH_FAIL
    }
};

export const logout = () => {
    document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;'
    return {
        type: actioonTypes.AUTH_LOGOUT
    }
};

export const authLogin = (username, password) => {
    return dispatch => {
        dispatch(authStart());
        axios.post('http://192.168.1.242:8000/rest-auth/login/', {
            username: username,
            password: password
        })
            .then(res => {
                const token = res.data.key;
                const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
                document.cookie = "name=token; path=/; expires=" + expirationDate.toUTCString();
                dispatch(authSuccess(token));
            })
            .catch(error => {
                dispatch(authFail(error))
            })
    }
};

// place this function in new file
function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

export const authCheckState = () => {
    return dispatch => {
        console.log('authCheckState');
        const token = getCookie('token');
        if (token === undefined) {
            dispatch(logout())
        } else {
            dispatch(authSuccess(token))
        }
    }
};