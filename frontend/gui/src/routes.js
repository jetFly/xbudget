import React from 'react'
import { Route } from 'react-router-dom'

import TransactionList from './components/TransactionListView'
import TransactionDetail from "./components/TransactionDetailView";
import TransactionForm from "./components/TransactionForm";
import UserLoginForm from './components/UserLoginForm'

const BaseRouter = () => (
    <div>
        <Route exact path='/' component={TransactionList}/>
        <Route exact path='/tran/:transactionID' component={TransactionDetail}/>
        <Route exact path='/spend' render={(props) => <TransactionForm requestType='add' transactionID={null}/>}/>
        <Route exact path='/income' component={TransactionForm} requestType={'income'}/>
        <Route exact path='/login' component={UserLoginForm}/>
    </div>
);

export default BaseRouter;