from rest_framework import serializers
from transactions.models import Transaction, TransactionCategory


class TransactionSerializer(serializers.ModelSerializer):
    transaction_type_code = serializers.ReadOnlyField(source='transaction_type.api_code')
    category_name = serializers.ReadOnlyField(source='category.name')

    class Meta:
        model = Transaction
        fields = (
            'id',
            'amount',
            'created',
            'transaction_type_code',
            'comment',
            'category_name',
            'category',
            'transaction_type'
        )


class TransactionCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionCategory
        fields = ('id', 'name')
