from rest_framework.routers import DefaultRouter
from transactions.api.views import TransactionViewSet, TransactionCategoryViewSet, BalanceView

router = DefaultRouter()
router.register(r'transactions', TransactionViewSet, base_name='transactions')
router.register(r'categories', TransactionCategoryViewSet, base_name='categories')
router.register(r'balance', BalanceView, base_name='balance')
urlpatterns = router.urls
