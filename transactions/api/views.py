from transactions.models import Transaction, TransactionCategory
from .serializers import TransactionSerializer, TransactionCategorySerializer
from rest_framework import viewsets
from rest_framework.views import APIView
from django.db.models import Sum

from django.db.models import Q
from rest_framework.response import Response


class TransactionViewSet(viewsets.ModelViewSet):
    serializer_class = TransactionSerializer

    def get_queryset(self):
        return Transaction.objects.filter(Q(user=self.request.user))

    def create(self, request, *args, **kwargs):
        initial_amount = int(request.data.get('amount'))
        amount = 0

        # transaction_type 1 means profit, 2 - loss
        if request.data.get('transaction_type') == 1:
            amount = abs(initial_amount)
        elif request.data.get('transaction_type') == 2:
            amount = abs(initial_amount) * -1

        request.data['amount'] = amount

        transaction = TransactionSerializer(data=request.data)
        if transaction.is_valid():
            transaction.save(user=request.user)
            return Response(status=201)
        else:
            return Response(status=400)


class TransactionCategoryViewSet(viewsets.ModelViewSet):
    serializer_class = TransactionCategorySerializer

    def get_queryset(self):
        category_type = self.request.query_params.get('category_type')
        return TransactionCategory.objects.filter(Q(category_type=category_type))


class BalanceView(viewsets.ViewSet):
    def list(self, request):
        transactions = Transaction.objects.filter(Q(user=request.user))
        balance = transactions.aggregate(Sum('amount'))['amount__sum']
        return Response({'balance': balance if balance else 0})
