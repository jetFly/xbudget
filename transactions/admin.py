from django.contrib import admin
from .models import Transaction, TransactionType, TransactionCategory


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'amount', 'transaction_type', 'category', 'comment', 'created')


admin.site.register(Transaction, TransactionAdmin)
admin.site.register(TransactionType)
admin.site.register(TransactionCategory)
