from django.db import models
from django.contrib.auth.models import User


class TransactionType(models.Model):
    name = models.CharField('Название', max_length=50)
    api_code = models.CharField('Код для API', max_length=1, default='C')

    class Meta:
        verbose_name = "Тип транзакции"
        verbose_name_plural = "Типы транзакций"

    def __str__(self):
        return self.name


class TransactionCategory(models.Model):
    name = models.CharField('Название', max_length=50)
    category_type = models.ForeignKey(
        TransactionType,
        verbose_name="Тип",
        on_delete=models.SET_NULL,
        null=True,
    )

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.name


class Transaction(models.Model):
    user = models.ForeignKey(
        User,
        verbose_name='Пользователь',
        on_delete=models.SET_NULL,
        null=True,
    )

    amount = models.DecimalField('Сумма', max_digits=8, decimal_places=2, default=0)
    created = models.DateTimeField('Дата создания', auto_now_add=True,
                                   null=True)
    transaction_type = models.ForeignKey(
        TransactionType,
        related_name='type_code',
        verbose_name="Тип",
        on_delete=models.SET_NULL,
        null=True,
    )
    category = models.ForeignKey(
        TransactionCategory,
        verbose_name="Категория",
        on_delete=models.SET_NULL,
        null=True,
    )
    comment = models.TextField('Комментарий', null=True, blank=True)

    class Meta:
        ordering = ['-created']
        verbose_name = "Транзакция"
        verbose_name_plural = "Транзакции"

    def __str__(self):
        return str(self.id)
